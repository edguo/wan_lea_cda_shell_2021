#!/bin/bash
#saisir fichier
#verify fichier
#fichier=F
#group=G
#user=U
back()
{
	echo "input enter "
	read
}
saisir_F()
{
	read -p "Saisir le nom de fichier." F
	if [ -z "$F" ]
	then
		echo "Saisir bien le nom de fichier."
		back
	fi
}
verify_F()
{
	if [ -f "$F" ]
	then
		echo "Le fichier $F existe."
	else
		echo "Le fichier $F n'existe pas."
		back
	fi
}
saisir_U()
{
	read -p " Saisir le nom de new user que vous voulez changer." U
	if [ -z "$U" ]
	then
		echo " Saisir new user, svp."
		back
	fi
}
verify_U()
{
	if grep "^$U:" /etc/passwd > /dev/null
	then
		echo "User $U existe."
	else
		echo "User $U n'existe pas."
		back
	fi
}
saisir_G()
{
	read -p "Saisir le nom du new group, vous voulez changer;" G
	if [ -z "$G" ]
	then
		echo "Saisir bien le nom du new group."
		back
	fi
}
verify_G()
{
	if grep "^$G:" /etc/group > /dev/null 
	then
		echo "Group $G existe."
	else
		echo "Group $G n'existe pas."
		back
	fi
}
REP=0
#Action=A
while [ "$REP" = 0 ]
do
	saisir_F
	verify_F
	saisir_U
	verify_U
	saisir_G
	verify_G

	read -p "Vous voulez [chown/chgrp/quit] " A	
	case $A in
		"chown")
		
			chown $U $F
			echo " $F a bien change	le user $U. "
			;;
		"chgrp")	
			
			chgrp $G $F
			echo "$F a bien change le group $G."
			
			;;
		"quit")
			exit
			;;
		*)	
			echo "error"
			;;
		esac
done
